import React from 'react';  
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import HomePage from "./Components/wallet_home_page";   
import Balancepage from "./Components/Balance_page";
import Transaction from './Components/Transaction';
//import { useHistory } from "react-router-dom";

// function HomeButton() {
//   let history = useHistory();
// }

function App() {

  return (   

      
<Router>        
  <Routes>
  
    <Route exact path='/' element={<HomePage/>}/> 
    <Route exact path='/Balancepage' element={<Balancepage/>}/>
    <Route exact path='/Transaction' element={<Transaction/>}/>
   
  </Routes>

</Router>
        
   
  )
}

export default App;
