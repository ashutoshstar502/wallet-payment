import React, { useState, useEffect } from 'react';  
import axios from 'axios';
import { Col, Container, Row} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

 function HomePage(){
      //console.log(props) 
      const navigate = useNavigate();
      const [wallet, setWallet] = useState({
        name: "",
        balance: 0,
      });
     
      useEffect(() => {
        const walletId = localStorage.getItem('walletId');
        if(walletId){
          console.log(walletId +" checking form if")
          navigate('/Balancepage');
        }       
      });
      const handleSubmit = async (e) => {
        // make a post request to the /wallet endpoint with the wallet data
        // on success, save the returned wallet id to local storage
        // and update the wallet state with the returned data
      
        e.preventDefault();
        console.log("checking default event value ", e.preventDefault())
    
        console.log("checking wallets details", wallet);
        try {
          const response = await axios.post('https://highlevel-wallet.onrender.com/setup', wallet);
          console.log("api response data", response.data.data);
          localStorage.setItem('walletId', response.data.data.id);
          setWallet({
            ...wallet,
            name: response.data.data.name,
            balance: response.data.data.balance
          }); 
          navigate('/Balancepage');        

        } catch (error) {
          console.error(error);
        }
      };

      const handleInputChange = (event) => {
        console.log("cheking events value", event);
        setWallet({
          ...wallet,
          [event.target.name]: event.target.value,
        });
      };
    
     return (
         <Container>
             <Row>
                 <Col></Col>
                 <Col className='mainPage'>
                            <div className='mainPageheader'>Wallet Initialization</div>
                            <div>
                                <form onSubmit={handleSubmit}>
                               <div style={{marginBottom:"10px"}}> 
                                        Username: <input type="text" name="name"  id="name" value={wallet.name}

onChange={handleInputChange} style={{marginLeft:"26px"}} /> 
                               </div>
                               <div style={{marginBottom:"10px"}}> 
                                        Initial Balance:  <input type="text"  id="balance" name="balance" value={wallet.balance} onChange={handleInputChange} />
                               </div>
                            <div style={{ marginBottom: "10px", textAlign: 'center' }}>
                               <button type='submit'>Submit </button>
                             </div>
                             </form>
                            </div>
                 </Col>
                 <Col></Col>
             </Row>
         </Container>
     )

}

export default HomePage;
