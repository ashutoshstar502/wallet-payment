import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Col, Container, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';

function Balancepage() {
    const navigate = useNavigate();
    const [balance, setBalance] = useState({
        name: "",
        balance: 0,
    });

    const [transactionAmount, setTransactionAmount] = useState('');
    const [transactionType, setTransactionType] = useState('CREDIT');
    const handleSubmit1 = async (event) => {
        event.preventDefault();
        console.log("checking transaction transactionAmount values 1", +transactionAmount)
        console.log("checking transaction description values 1", transactionType)

        try {
            const walletId = localStorage.getItem('walletId');

            const response = await axios.post(`https://highlevel-wallet.onrender.com/transact/${walletId}`, {
                amount: transactionType === 'CREDIT' ? +transactionAmount : -transactionAmount,
                description: transactionType
            });
            console.log("after transaction api response", response.data);
            setBalance({
                ...balance,
                // name: response.data.name,
                balance: response.data.data.balance
            });
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        const walletId = localStorage.getItem('walletId');
        if(!walletId){
          console.log(walletId +" checking form if")
          navigate('/');
        }              
        const fetchWallet = async () => {
            const response = await axios.get(`https://highlevel-wallet.onrender.com/wallet/${walletId}`);
            //const transactionData = await axios.get(`https://highlevel-wallet.onrender.com/transactions/?walletId=${walletId}`);

            console.log(response, "checking wallet api response")
            //console.log(transactionData, "checking all transactionData api response")

            setBalance({
                ...balance,
                name: response.data.data.name,
                balance: response.data.data.balance
            });
            // setData(transactionData.data)
            //props.history.push('/Transaction');
        };
        fetchWallet();
    }, []);

    function ministatement(){
        navigate('/Transaction');
    } 

    return (
        <Container>
            <Row>
                <Col></Col>
                <Col className='mainPage'>                   
                    <div className='mainPageheader'>
                        <h4>Welcome, {balance.name}</h4>
                        <p>Your current balance is {balance.balance}</p>
                    </div>
                    <div style={{marginBottom: "10px"}}>Make your transaction here. </div>
                    
                    <div>
                        <form onSubmit={handleSubmit1}>
                            <div>
                                <label htmlFor="amount">Amount:</label>
                                <input
                                    type="text"
                                    id="amount"
                                    value={transactionAmount}
                                    onChange={(event) => setTransactionAmount(event.target.value)}
                                    style={{ marginLeft: "10px", marginBottom: "10px" }} />
                            </div>
                            <div>
                                <label htmlFor="type">Type:</label>
                                <select
                                    id="type"
                                    value={transactionType}
                                    onChange={(event) => setTransactionType(event.target.value)}
                                    style={{ marginLeft: "35px", marginBottom: "10px" }} >
                                    <option value="CREDIT">Credit</option>
                                    <option value="DEBIT">Debit</option>
                                </select>
                            </div>
                            <div style={{ textAlign: 'center', marginBottom: "10px" }}>
                                <button type="submit">Submit</button>
                            </div>
                        
                        </form>
                        <div style={{textAlign:'center', marginBottom: "10px" }}>
                            <button type="button" onClick={ministatement}>Check your ministatement</button></div>
                    </div>
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}

export default Balancepage;