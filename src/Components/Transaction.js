import React, { useState, useEffect} from 'react';  
import { saveAs } from 'file-saver';
import axios from 'axios';
import { Col, Container, Row} from 'react-bootstrap';

function Transaction(){
    const [data, setData] = useState([]);
    const [name, setName] = useState({name:""});
      
    const handleExport = () => {
        const csvData = data.map((item) => [item._id, item.amount, item.description, item.created_at].join(',')).join('\n');
        const blob = new Blob([csvData], { type: 'text/csv;charset=utf-8;' });
        saveAs(blob, 'data.csv');
      };
    
      useEffect(() => {
        const walletId = localStorage.getItem('walletId');
        const fetchTransaction = async () => {
            const response = await axios.get(`https://highlevel-wallet.onrender.com/wallet/${walletId}`);
            const transactionData = await axios.get(`https://highlevel-wallet.onrender.com/transactions/?walletId=${walletId}`);

            //console.log(response, "checking wallet api response")
            console.log(transactionData, "checking all transactionData api response")

            setName({    
                ...name,           
                name: response.data.data.name              
            });
            setData(transactionData.data.transactions)
        };
        fetchTransaction();
    }, []);

    return(
        
        <div className="container mx-auto mt-5">
            <div>
                Hello, {name.name} your transaction detail         
            </div>
      <table style={{width:'100%',marginBottom:'10px'}} border='1'>
        <thead>
          <tr>
            <th>TransactionID</th>
            <th>Amount</th>
            <th>Description</th>
            <th>CreatedDate</th>
          </tr>
        </thead>
        <tbody>
        {data.map((item) => (
            <tr key={item._id}>
              <td>{item._id}</td>
              <td>{item.amount}</td>
              <td>{item.description}</td>
              <td>{item.created_at}</td>
            </tr>
          ))}
        </tbody>       
      </table>
       
            <button onClick={handleExport}>Export to CSV</button>
</div>

    )
}

export default Transaction;